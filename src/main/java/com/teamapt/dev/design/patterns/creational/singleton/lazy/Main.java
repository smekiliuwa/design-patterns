package com.teamapt.dev.design.patterns.creational.singleton.lazy;

public class Main {

    public static void main(String[] args) {

        for (int i = 0; i < 20; i++) {
            SomeOtherClass someOtherClass = new SomeOtherClass();
            System.out.println("SomeOtherClass hash code (lazy init): " + someOtherClass.hashCode());

            Singleton singleton = Singleton.getInstance();
            System.out.println("Singleton hash code (lazy init): " + singleton.hashCode());
            System.out.println();
        }
    }

    static class SomeOtherClass {

    }
}
