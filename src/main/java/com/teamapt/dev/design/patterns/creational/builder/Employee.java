package com.teamapt.dev.design.patterns.creational.builder;

public class Employee implements Cloneable {

    private final String name;
    private final int age;
    private final String dateOfBirth;
    private final String position;

    public static class Builder {
        private String name;
        private int age;
        private String dateOfBirth;
        private String position;

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder age(int age) {
            this.age = age;
            return this;
        }

        public Builder dateOfBirth(String dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
            return this;
        }

        public Builder position(String position) {
            this.position = position;
            return this;
        }

        public Employee build() {
            return new Employee(this);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    protected Employee(Builder builder) {
        this.name = builder.name;
        this.age = builder.age;
        this.dateOfBirth = builder.dateOfBirth;
        this.position = builder.position;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", position='" + position + '\'' +
                '}';
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
