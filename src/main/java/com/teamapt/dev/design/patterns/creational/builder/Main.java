package com.teamapt.dev.design.patterns.creational.builder;

public class Main {

    public static final String NAME = "Nameless";
    public static final int AGE = 12;
    public static final String DATE_OF_BIRTH = "10-10-2010";
    public static final String POSITION = "CEO";

    public static void main(String[] args) {
        buildEmployeeWithNameAlone();

        buildEmployeeWithNameAndAge();

        buildEmployeeWithNameAndBirthDate();

        buildEmployeeWithAllRequiredInfo();
    }

    private static void buildEmployeeWithNameAlone() {
        Employee nameOnly = Employee.builder()
                .name(NAME)
                .build();
        System.out.println(nameOnly);
    }

    private static void buildEmployeeWithNameAndAge() {
        Employee nameAndAge = Employee.builder()
                .name(NAME)
                .age(AGE)
                .build();
        System.out.println(nameAndAge);
    }

    private static void buildEmployeeWithNameAndBirthDate() {
        Employee nameAndBirthDate = Employee.builder()
                .name(NAME)
                .dateOfBirth(DATE_OF_BIRTH)
                .build();
        System.out.println(nameAndBirthDate);
    }

    private static void buildEmployeeWithAllRequiredInfo() {
        Employee allInfo = Employee.builder()
                .name(NAME)
                .age(AGE)
                .dateOfBirth(DATE_OF_BIRTH)
                .position(POSITION)
                .build();
        System.out.println(allInfo);
    }
}


