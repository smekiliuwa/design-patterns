package com.teamapt.dev.design.patterns.creational.abs.factory;

import com.teamapt.dev.design.patterns.creational.factory.Shape;
import com.teamapt.dev.design.patterns.creational.factory.enums.ShapeType;

public class RoundedSquare implements Shape {
    public void draw() {
        System.out.println("Now drawing a rounded square...");
    }

    public ShapeType getType() {
        return ShapeType.ROUNDED_SQUARE;
    }
}


