package com.teamapt.dev.design.patterns.creational.singleton.lazy.thread.safe;

public class Main {

    public static void main(String[] args) {
        Thread naiveThread1 = new Thread(new NaiveSingletonThread(1));
        Thread safeThread1 = new Thread(new ThreadSafeSingletonThread(1));
        Thread naiveThread2 = new Thread(new NaiveSingletonThread(2));
        Thread safeThread2 = new Thread(new ThreadSafeSingletonThread(2));

        naiveThread1.start();
        safeThread1.start();
        naiveThread2.start();
        safeThread2.start();
    }

    static class NaiveSingletonThread implements Runnable {
        private final int id;

        public NaiveSingletonThread(int id) {
            this.id = id;
        }

        @Override
        public void run() {
            NaiveSingleton singleton = NaiveSingleton.getInstance();
            System.out.printf("Naive Singleton %d hash code: %s%n", id, singleton.hashCode());
            System.out.println();
        }
    }

    static class ThreadSafeSingletonThread implements Runnable {
        private final int id;

        public ThreadSafeSingletonThread(int id) {
            this.id = id;
        }

        @Override
        public void run() {
            ThreadSafeSingleton singleton = ThreadSafeSingleton.getInstance();
            System.out.printf("Thread Safe Singleton %d hash code: %s%n", id, singleton.hashCode());
            System.out.println();
        }
    }
}
