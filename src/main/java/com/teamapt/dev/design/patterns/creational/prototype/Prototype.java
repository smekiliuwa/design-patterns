package com.teamapt.dev.design.patterns.creational.prototype;

public interface Prototype {
    Employee getClone();
}
