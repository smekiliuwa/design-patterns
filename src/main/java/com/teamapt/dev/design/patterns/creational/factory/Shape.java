package com.teamapt.dev.design.patterns.creational.factory;

import com.teamapt.dev.design.patterns.creational.factory.enums.ShapeType;

public interface Shape {
    void draw();

    ShapeType getType();
}


