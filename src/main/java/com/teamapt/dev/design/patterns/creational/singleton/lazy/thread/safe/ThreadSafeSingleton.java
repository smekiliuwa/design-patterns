package com.teamapt.dev.design.patterns.creational.singleton.lazy.thread.safe;

@SuppressWarnings("ALL")
public class ThreadSafeSingleton {

    public static ThreadSafeSingleton instance;

    private ThreadSafeSingleton() {
        // Long running operation
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ignored) {

        }
    }

    public static ThreadSafeSingleton getInstance() {
        synchronized(ThreadSafeSingleton.class) {
            if (instance == null) {
                instance = new ThreadSafeSingleton();
            }
            return instance;
        }
    }
}
