package com.teamapt.dev.design.patterns.creational.object.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnectionPool extends ObjectPool<Connection> {

    private final String dsn, username, password;

    public JDBCConnectionPool(String driver, String dsn, String username, String password) {
        super();
        try {
            Class.forName(driver).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        this.dsn = dsn;
        this.username = username;
        this.password = password;
    }

    @Override
    protected Connection create() {
        try {
            return (DriverManager.getConnection(dsn, username, password));
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public void expire(Connection o) {
        try {
            o.close();
            System.out.println("Connection expired: " + o.hashCode());
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean validate(Connection o) {
        try {
            return !o.isClosed();
        } catch (SQLException e) {
            e.printStackTrace();
            return (false);
        }
    }

    @Override
    public String toString() {
        return "JDBCConnectionPool{" +
                "dsn='" + dsn + '\'' +
                ", username='" + username + '\'' +
                ", password='***'" +
                '}';
    }
}
