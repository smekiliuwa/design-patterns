package com.teamapt.dev.design.patterns.creational.object.pool;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static final String CREATE_SCHEMA_SQL = "CREATE TABLE customer (id BIGINT, first_name VARCHAR(100), last_name VARCHAR(100))";
    public static final String INSERT_SQL_1 = "INSERT INTO customer (id, first_name, last_name) VALUES(1, 'Alpha', 'Beta')";
    public static final String INSERT_SQL_2 = "INSERT INTO customer (id, first_name, last_name) VALUES(2, 'Gamma', 'Zeta')";
    public static final String SELECT_SQL_1 = "SELECT * FROM customer WHERE id=1";
    public static final String SELECT_SQL_2 = "SELECT * FROM customer WHERE id=2";
    public static final String DELETE_SCHEMA_SQL = "DROP TABLE customer";

    private static final String driver = "com.mysql.cj.jdbc.Driver";

    public static void main(String[] args) throws SQLException, InterruptedException {
        JDBCConnectionPool pool = getConnectionPool();

        System.out.println(pool);
        executeStatement(pool, CREATE_SCHEMA_SQL);

        ExecutorService executorService = Executors.newFixedThreadPool(2);

        List<Callable<Void>> tasks = getInsertTasks(pool);
        executorService.invokeAll(tasks);

        tasks = getQueryTasks(pool);
        executorService.invokeAll(tasks);

        executeStatement(pool, DELETE_SCHEMA_SQL);
    }

    private static List<Callable<Void>> getInsertTasks(JDBCConnectionPool pool) {
        List<Callable<Void>> tasks = new ArrayList<>();
        tasks.add(() -> {
            executeStatement(pool, INSERT_SQL_1);
            return null;
        });
        tasks.add(() -> {
            executeStatement(pool, INSERT_SQL_2);
            return null;
        });
        return tasks;
    }
    private static List<Callable<Void>> getQueryTasks(JDBCConnectionPool pool) {
        List<Callable<Void>> tasks = new ArrayList<>();
        tasks.add(() -> {
            executeSelectQuery(pool, SELECT_SQL_1);
            return null;
        });
        tasks.add(() -> {
            executeSelectQuery(pool, SELECT_SQL_2);
            return null;
        });
        return tasks;
    }

    private static JDBCConnectionPool getConnectionPool() {
        return new JDBCConnectionPool(
                driver,
                "jdbc:mysql://localhost:3306/object_pool_demo?createDatabaseIfNotExist=true&autoReconnect=true&useSSL=false&serverTimezone=UTC",
                "root", "root");
    }

    private static void executeStatement(JDBCConnectionPool pool, String sql) throws SQLException {
        final Connection connection = pool.checkOut();

        // use connection
        Statement statement = connection.createStatement();
        statement.execute(sql);

        // Return the connection
        pool.checkIn(connection);
    }

    private static void executeSelectQuery(JDBCConnectionPool pool, String sql) throws SQLException {
        final Connection connection = pool.checkOut();

        // use connection
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");

            Customer customer = new Customer(id, firstName, lastName);
            System.out.println("Customer fetched: " + customer);
        }

        // Return the connection
        pool.checkIn(connection);
    }
}
