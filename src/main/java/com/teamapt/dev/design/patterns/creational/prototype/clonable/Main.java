package com.teamapt.dev.design.patterns.creational.prototype.clonable;

public class Main {

    public static void main(String[] args) throws CloneNotSupportedException {
        Employee original = new Employee(1, "Sam", 27, "19-02-1995", "Engineer");

        Employee clone = (Employee) original.clone();
        System.out.println("Clone of \n" + original + "\nis\n" + clone);
    }
}
