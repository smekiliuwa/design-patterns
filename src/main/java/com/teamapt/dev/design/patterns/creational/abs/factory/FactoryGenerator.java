package com.teamapt.dev.design.patterns.creational.abs.factory;

import com.teamapt.dev.design.patterns.creational.factory.enums.ShapeType;

public class FactoryGenerator {

    public static AbstractShapeFactory getFactory(ShapeType shapeType){
        if(isRounded(shapeType)){
            return new RoundedShapeFactory();
        } else {
            return new ShapeFactory();
        }
    }

    private static boolean isRounded(ShapeType shapeType) {
        return shapeType.name().startsWith("ROUNDED_");
    }
}

