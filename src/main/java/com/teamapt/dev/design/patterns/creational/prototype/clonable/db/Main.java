package com.teamapt.dev.design.patterns.creational.prototype.clonable.db;

import com.teamapt.dev.design.patterns.creational.prototype.clonable.Employee;

import java.util.HashMap;
import java.util.Random;

public class Main {

    private static final Random random = new Random();

    private static final HashMap<Long, Employee> employeeCache = new HashMap<Long, Employee>();

    public static void main(String[] args) throws CloneNotSupportedException {
        long id = getRandomId();
        for (int i = 0; i < 10; i++) {
            Employee employee = findEmployeeById(id);
            System.out.println("Employee " + employee + "  Hash code: " + employee.hashCode());
        }
    }

    private static long getRandomId() {
        return (randomLong() % 100) + 1;
    }

    private static long randomLong() {
        long nextLong = random.nextLong();
        if (nextLong < 0) {
            nextLong *= -1;
        }
        return nextLong;
    }

    private static Employee findEmployeeById(Long id) throws CloneNotSupportedException {
        if (!employeeCache.containsKey(id)) {
            Employee employee = findEmployeeByIdFromDatabase(id);
            // cache prototype
            employeeCache.put(id, employee);
        }
        return (Employee) employeeCache.get(id).clone();
    }

    private static Employee findEmployeeByIdFromDatabase(Long id) {
        // Slow database query to find employee
        try {
            System.out.println("Fetching id " + id + " from database...");
            long startTime = System.currentTimeMillis();

            // Slow database query
            Thread.sleep(3000 + (randomLong() % 5000));

            long endTime = System.currentTimeMillis();
            System.out.println("Record fetched in " + (endTime - startTime) / 1000 + " seconds");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        return new Employee(id, "Test Employee", 35, "10-04-1987", "Admin");
    }
}
