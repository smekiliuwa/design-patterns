package com.teamapt.dev.design.patterns.creational.factory;

import com.teamapt.dev.design.patterns.creational.factory.enums.ShapeType;

public class Square implements Shape {
    public void draw() {
        System.out.println("Now drawing a square...");
    }

    public ShapeType getType() {
        return ShapeType.SQUARE;
    }
}

