package com.teamapt.dev.design.patterns.creational.singleton.lazy.thread.safe;

@SuppressWarnings("ALL")
public class NaiveSingleton {

    public static NaiveSingleton instance;

    private NaiveSingleton() {
        // Long running operation
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ignored) {

        }
    }

    public static NaiveSingleton getInstance() {
        if (instance == null) {
            instance = new NaiveSingleton();
        }
        return instance;
    }
}
