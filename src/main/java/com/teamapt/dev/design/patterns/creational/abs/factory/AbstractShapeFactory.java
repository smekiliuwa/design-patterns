package com.teamapt.dev.design.patterns.creational.abs.factory;

import com.teamapt.dev.design.patterns.creational.factory.Shape;
import com.teamapt.dev.design.patterns.creational.factory.enums.ShapeType;

public abstract class AbstractShapeFactory {
    abstract Shape getShape(ShapeType shapeType);
}


