package com.teamapt.dev.design.patterns.creational.abs.factory;

import com.teamapt.dev.design.patterns.creational.factory.Shape;
import com.teamapt.dev.design.patterns.creational.factory.enums.ShapeType;

public class RoundedRectangle implements Shape {
    public void draw() {
        System.out.println("Now drawing a rounded rectangle...");
    }

    public ShapeType getType() {
        return ShapeType.ROUNDED_RECTANGLE;
    }
}

