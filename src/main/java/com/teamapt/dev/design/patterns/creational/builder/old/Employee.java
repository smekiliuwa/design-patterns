package com.teamapt.dev.design.patterns.creational.builder.old;

public class Employee {

    private final String name;
    private int age;
    private String dateOfBirth;
    private String position;

    public Employee(String name) {
        this.name = name;
    }

    public Employee(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Employee(String name, String dateOfBirth, String position) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.position = position;
    }

    public Employee(String name, int age, String dateOfBirth, String position) {
        this.name = name;
        this.age = age;
        this.dateOfBirth = dateOfBirth;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getPosition() {
        return position;
    }
}



