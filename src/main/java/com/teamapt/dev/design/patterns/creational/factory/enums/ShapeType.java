package com.teamapt.dev.design.patterns.creational.factory.enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum ShapeType {
    RECTANGLE, SQUARE, TRIANGLE, ROUNDED_RECTANGLE, ROUNDED_SQUARE;

    private static final List<ShapeType> VALUES =
            Collections.unmodifiableList(Arrays.asList(values()));
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    public static ShapeType randomShape()  {
        return VALUES.get(RANDOM.nextInt(3));
    }
    public static ShapeType randomShape(boolean canBeRounded)  {
        return VALUES.get(RANDOM.nextInt(canBeRounded ? SIZE : 3));
    }
}
