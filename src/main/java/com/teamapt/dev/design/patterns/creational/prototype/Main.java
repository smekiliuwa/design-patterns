package com.teamapt.dev.design.patterns.creational.prototype;

public class Main {

    public static void main(String[] args) {
        Employee original = new Employee(1, "Sam", 27, "19-02-1995", "Engineer");

        Employee clone = original.getClone();
        System.out.println("Clone of \n" + original + "\nis\n" + clone);
    }
}
