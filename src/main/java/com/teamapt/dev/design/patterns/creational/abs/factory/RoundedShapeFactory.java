package com.teamapt.dev.design.patterns.creational.abs.factory;

import com.teamapt.dev.design.patterns.creational.factory.Shape;
import com.teamapt.dev.design.patterns.creational.factory.enums.ShapeType;

public class RoundedShapeFactory extends AbstractShapeFactory {

    public Shape getShape(ShapeType shapeType){
        if(ShapeType.ROUNDED_SQUARE.equals(shapeType)){
            return new RoundedSquare();

        } else if(ShapeType.ROUNDED_RECTANGLE.equals(shapeType)){
            return new RoundedRectangle();

        }

        throw new RuntimeException("Invalid shapeType");
    }
}






