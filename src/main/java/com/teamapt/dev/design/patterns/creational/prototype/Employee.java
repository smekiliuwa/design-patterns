package com.teamapt.dev.design.patterns.creational.prototype;

public class Employee implements Prototype {

    private final long id;
    private final String name;
    private final int age;
    private final String dateOfBirth;
    private final String position;

    public Employee(long id, String name, int age,
                    String dateOfBirth, String position) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.dateOfBirth = dateOfBirth;
        this.position = position;
    }

    @Override
    public Employee getClone() {
        return new Employee(id, name, age, dateOfBirth, position);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", position='" + position + '\'' +
                '}';
    }
}

