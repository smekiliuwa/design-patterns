package com.teamapt.dev.design.patterns.creational.singleton.early;

@SuppressWarnings("ALL")
public class Singleton {

    public static final Singleton instance = new Singleton();

    private Singleton() {}

    public static Singleton getInstance() {
        return instance;
    }
}
