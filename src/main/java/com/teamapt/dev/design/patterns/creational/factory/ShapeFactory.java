package com.teamapt.dev.design.patterns.creational.factory;

import com.teamapt.dev.design.patterns.creational.factory.enums.ShapeType;

public class ShapeFactory {

    public Shape getShape(ShapeType shapeType){
        if(ShapeType.SQUARE.equals(shapeType)){
            return new Square();

        } else if(ShapeType.RECTANGLE.equals(shapeType)){
            return new Rectangle();

        } else if(ShapeType.TRIANGLE.equals(shapeType)){
            return new Triangle();
        }

        throw new RuntimeException("Invalid shapeType");
    }
}

