package com.teamapt.dev.design.patterns.creational.factory;

import com.teamapt.dev.design.patterns.creational.factory.enums.ShapeType;

public class Main {

    private static final ShapeFactory shapeFactory = new ShapeFactory();

    public static void main(String[] args) {
        Shape randomShape;
        for (int i = 0; i < 20; i++) {
            randomShape = getRandomShape();
            System.out.println("Shape: " + randomShape.getType());

            randomShape.draw();
            System.out.println();
        }
    }

    private static Shape getRandomShape() {
        ShapeType randomShape = ShapeType.randomShape();
        return shapeFactory.getShape(randomShape);
    }
}
