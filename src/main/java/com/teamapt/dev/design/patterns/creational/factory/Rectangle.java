package com.teamapt.dev.design.patterns.creational.factory;

import com.teamapt.dev.design.patterns.creational.factory.enums.ShapeType;

public class Rectangle implements Shape {
    public void draw() {
        System.out.println("Now drawing a rectangle...");
    }

    public ShapeType getType() {
        return ShapeType.RECTANGLE;
    }
}

