package com.teamapt.dev.design.patterns.creational.singleton.lazy;

@SuppressWarnings("ALL")
public class Singleton  {

    public static Singleton instance;

    private Singleton() {}

    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }
}




