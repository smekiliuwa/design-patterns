package com.teamapt.dev.design.patterns.creational.factory;

import com.teamapt.dev.design.patterns.creational.factory.enums.ShapeType;

public class Triangle implements Shape {
    public void draw() {
        System.out.println("Now drawing a triangle...");
    }

    public ShapeType getType() {
        return ShapeType.TRIANGLE;
    }
}

